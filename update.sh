KDASHBOARD_CHART_VERSION="5.10.0"
KDASHBOARD_NAMESPACE="kubernetes-dashboard"

# ------------------------------------------------

# Create namespace
kubectl create ns "${KDASHBOARD_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -

# Install Cert Manager
helm repo add "kubernetes-dashboard" "https://kubernetes.github.io/dashboard"
helm repo update
helm upgrade --install --atomic --debug "kubernetes-dashboard" "kubernetes-dashboard/kubernetes-dashboard" \
  --version "${KDASHBOARD_CHART_VERSION}" \
  --namespace "${KDASHBOARD_NAMESPACE}" \
  --timeout 600s \
  --values "values.yml"

# Check status
kubectl -n "${KDASHBOARD_NAMESPACE}" rollout status deployment kubernetes-dashboard

# Apply RBAC
kubectl apply -f manifests.yml