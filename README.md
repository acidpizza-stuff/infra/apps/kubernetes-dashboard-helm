## Reference

- https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard
- https://github.com/kubernetes/dashboard


## Update Kubernetes Dashboard

Manual actions typically needed for major version upgrade.

```bash
helm repo add "kubernetes-dashboard" "https://kubernetes.github.io/dashboard"
helm search repo kubernetes-dashboard/kubernetes-dashboard -l

./update.sh
```


## Access Kubernetes Dashboard

Need to use token to access dashboard. The dashboard needs the user in the kubeconfig file to have either username+password or token, but the default kubeconfig file only contains client certificates.

```bash
## Token API
kubectl -n kubernetes-dashboard create token readonly-sa

## Long-lived tokens (not recommended)
# get service account token (readonly)
kubectl -n kubernetes-dashboard describe secret readonly-sa | grep token: | tr -d ' ' | cut -d ':' -f 2
# get service account token (admin) - Not set
kubectl -n kubernetes-dashboard describe secret kubernetes-dashboard | grep token: | tr -d ' ' | cut -d ':' -f 2
```

Alternate way to access kubernetes-dashboard.

```bash
# start kube proxy
kubectl proxy

# access dashboard from browser
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:https/proxy/#/login
```
